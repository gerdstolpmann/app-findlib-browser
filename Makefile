NAME=findlib-browser
BINDIR=/opt/ocaml-3.04/html
VERSION=0.2.3

browser: browser.ml
	ocamlfind ocamlc -o browser -package findlib,unix,str,netstring,netcgi2 \
		-linkpkg browser.ml

browser.ml: browser.cgi
	awk -f unscript.awk browser.cgi >browser.ml

.PHONY: install
install:
	cp browser $(BINDIR)/browser.cgi

.PHONY: clean
clean:
	rm -f *.cmi *.cmo *.cma *.cmx *.o *.a *.cmxa browser.ml browser

.PHONY: distclean
distclean: clean
	rm -f *~ depend depend.pkg browser

version:
	@echo $(VERSION)
